/**
 * (c) 2013 TRIOLOGY GmbH
 */
/**
 * This package contains classes for the SCM-Checkstyle-Plugin.
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
package de.triology.scm.plugins.checkstyle;