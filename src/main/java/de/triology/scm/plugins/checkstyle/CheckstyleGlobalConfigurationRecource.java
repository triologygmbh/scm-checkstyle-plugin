/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.checkstyle;

import java.io.File;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.security.Role;

/**
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
@Path("plugins/checkstyle")
public class CheckstyleGlobalConfigurationRecource {

	/** the logger for CheckstyleConfigurationRecource. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckstyleGlobalConfigurationRecource.class);

	/** The global context. */
	private final CheckstyleGlobalContext context;

	// ~--- constructors ---------------------------------------------------------

	/**
	 * Constructs ...
	 * 
	 * @param pContext CheckstyleGlobalContext
	 */
	@Inject
	public CheckstyleGlobalConfigurationRecource(final CheckstyleGlobalContext pContext) {

		if (!SecurityUtils.getSubject().hasRole(Role.ADMIN)) {
			LOGGER.warn("user has not enough privileges to configure checkstyle");

			throw new WebApplicationException(Status.FORBIDDEN);
		}

		context = pContext;
	}

	/**
	 * update configuration.
	 * 
	 * @param configuration Checkstyle configuration
	 * @return Response
	 */
	@POST
	@Path("global-config")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public final Response updateConfiguration(final CheckstyleGlobalConfiguration configuration) {

		return setConfiguration(configuration);
	}

	/**
	 * @param configuration Checkstyle global configuration
	 * @return Response
	 */
	private Response setConfiguration(final CheckstyleGlobalConfiguration configuration) {
		boolean configIsFile = new File(configuration.getConfigPath()).isFile();
		if (configIsFile) {
			context.setConfiguration(configuration);
			return Response.ok().build();
		} else {
			LOGGER.warn("File or Folder ist not exist - configuration {}", configuration.getConfigPath());
			return Response.notAcceptable(null).build();
		}
	}

	/**
	 * get configuration.
	 * 
	 * @return Response
	 */
	@GET
	@Path("global-config")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public final Response getConfiguration() {

		return Response.ok(context.getConfiguration()).build();
	}

	/**
	 * Test Checkstyle.
	 * 
	 * @return Response
	 */
	@GET
	@Path("test")
	@Produces(MediaType.TEXT_PLAIN)
	public final Response getCheckResponse() {
		return testCheckstyle();
	}

	/**
	 * @return Response
	 */
	private Response testCheckstyle() {
		CheckstyleGlobalConfiguration configuration = context.getConfiguration();
		String srcPath = configuration.getSrcPath();
		String configPathTest = configuration.getConfigPathTest();
		boolean srcExists = new File(srcPath).exists();
		boolean configTestIsFile = new File(configPathTest).isFile();
		if (configTestIsFile && srcExists) {
			CheckstyleLoader check = new CheckstyleLoader(configPathTest, srcPath);
			if (new File(srcPath).isDirectory()) {
				check.setBaseDir(srcPath);
			}
			check.run();

			if (check.getEndState() == CheckstyleLoader.NO_ERRORS) {
				return Response.ok(check.getFileNames()).build();
			} else {
				return Response.ok(check.getResult()).build();
			}
		} else {
			LOGGER.warn("File or Folder is not exist - source {} and configuration {}", srcPath, configPathTest);
			return Response.notAcceptable(null).build();
		}
	}
}
