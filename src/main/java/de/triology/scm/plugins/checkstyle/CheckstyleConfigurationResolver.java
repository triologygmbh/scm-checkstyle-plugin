/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.checkstyle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.repository.Repository;

/**
 * Class to switch between global and repository configuration.
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
public final class CheckstyleConfigurationResolver {

	/**
	 * the logger for CheckstyleConfigurationResolver.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckstyleConfigurationResolver.class);

	/**
	 * Constructor.
	 */
	private CheckstyleConfigurationResolver() {
	}

	// ~--- methods --------------------------------------------------------------

	/**
	 * Dis-/Enable repository configuration.
	 * 
	 * @param context CheckstyleGlobalContext
	 * @param repository Repository
	 * 
	 * @return CheckstyleRepoConfiguration
	 */
	public static CheckstyleConfiguration resolve(final CheckstyleGlobalContext context, final Repository repository) {
		CheckstyleConfiguration configuration;

		if (context.getConfiguration().isDisableRepositoryConfiguration()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("repository configuration is disabled, try to use global configuration");
			}
			configuration = context.getConfiguration();
		} else {
			configuration = new CheckstyleConfiguration(repository);

			if (!configuration.isValid()) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("repository configuration is not valid, try to use global configuration");
				}
				configuration = context.getConfiguration();
			}
		}
		return configuration;
	}
}
