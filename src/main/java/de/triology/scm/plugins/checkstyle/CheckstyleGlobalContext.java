/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.checkstyle;

//~--- non-JDK imports --------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.store.Store;
import sonia.scm.store.StoreFactory;
import sonia.scm.util.AssertUtil;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
@Singleton
public class CheckstyleGlobalContext {

	/** Name of the store. */
	private static final String NAME = "checkstyle";

	/**
	 * the logger for CheckstyleGlobalContext.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CheckstyleGlobalContext.class);

	// ~--- fields ---------------------------------------------------------------

	/** Checkstyle configuration. */
	private CheckstyleGlobalConfiguration configuration;

	/** Field description. */
	private final Store<CheckstyleGlobalConfiguration> store;

	// ~--- constructors ---------------------------------------------------------

	/**
	 * Constructs ...
	 * 
	 * @param storeFactory StoreFactory
	 */
	@Inject
	public CheckstyleGlobalContext(final StoreFactory storeFactory) {
		store = storeFactory.getStore(CheckstyleGlobalConfiguration.class, NAME);
		configuration = store.get();

		if (configuration == null) {
			configuration = new CheckstyleGlobalConfiguration();
		}
	}

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Getting configuration.
	 * 
	 * @return CheckstyleConfiguration
	 */
	public final CheckstyleGlobalConfiguration getConfiguration() {
		return configuration;
	}

	// ~--- set methods ----------------------------------------------------------

	/**
	 * Setting configuration.
	 * 
	 * @param pConfiguration CheckstyleConfiguration
	 */
	public final void setConfiguration(final CheckstyleGlobalConfiguration pConfiguration) {
		LOG.debug("store checkstyle configuration");
		AssertUtil.assertIsValid(pConfiguration);
		this.configuration = pConfiguration;
		store.set(pConfiguration);
	}

}
