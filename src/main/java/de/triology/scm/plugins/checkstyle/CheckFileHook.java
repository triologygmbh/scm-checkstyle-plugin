/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.checkstyle;

//~--- non-JDK imports --------------------------------------------------------

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.plugin.ext.Extension;
import sonia.scm.repository.Changeset;
import sonia.scm.repository.Modifications;
import sonia.scm.repository.PreReceiveRepositoryHook;
import sonia.scm.repository.Repository;
import sonia.scm.repository.RepositoryHookEvent;
import sonia.scm.repository.api.CatCommandBuilder;
import sonia.scm.repository.api.RepositoryService;
import sonia.scm.repository.api.RepositoryServiceFactory;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.io.Closeables;
import com.google.inject.Inject;

/**
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
@Extension
public class CheckFileHook extends PreReceiveRepositoryHook {

	/** the logger for CheckCodeHook. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckFileHook.class);

	/** Checkstyle. */
	private CheckstyleLoader check;

	/** RepositoryServiceFactory. */
	private final RepositoryServiceFactory repositoryServiceFactory;

	/** Checkstyle configuration. */
	private CheckstyleConfiguration configuration;

	/** The global Checkstyle context. */
	private final CheckstyleGlobalContext context;

	/** Revision id. */
	private String newRevision = "";

	// ~--- constructors
	// ---------------------------------------------------------

	/**
	 * Constructs ...
	 * 
	 * @param pContext CheckstyleGlobalContext
	 * @param pRepositoryServiceFactory RepositoryServiceFactory
	 */
	@Inject
	public CheckFileHook(final CheckstyleGlobalContext pContext,
			final RepositoryServiceFactory pRepositoryServiceFactory) {
		context = pContext;
		repositoryServiceFactory = pRepositoryServiceFactory;
	}

	// ~--- methods
	// --------------------------------------------------------------

	/**
	 * Check code on repository hook event and get result.
	 * 
	 * @param event RepositoryHookEvent
	 */
	@Override
	public final void onEvent(final RepositoryHookEvent event) {
		Repository repository = event.getRepository();

		if (repository != null) {
			configuration = CheckstyleConfigurationResolver.resolve(context, repository);
			String pushReaction = configuration.getPushReaction();

			if (pushReaction.equals(CheckInReaction.NO_CHECK.name())) {
				LOGGER.info("checkstyle IS OFF.");
			} else {
				Iterable<String> changedFiles = collectModifiedFiles(repository, event.getChangesets());
				List<File> pFiles = new LinkedList<File>();
				check = new CheckstyleLoader(configuration.getConfigPath(), pFiles);

				if (check != null) {

					checkCode(repository, changedFiles);

					if (pushReaction.equals(CheckInReaction.CHECK_AND_WARN.name())) {
						LOGGER.info("checkstyle WARN: checkstyle has warnings or errors detected.");

						// sendWarning(check.getResult()); //TODO

					} else if (pushReaction.equals(CheckInReaction.CHECK_AND_BLOCK.name())
							&& check.getEndState() == CheckstyleLoader.ERRORS) {
						LOGGER.info("checkstyle PUSH BLOCK: checkstyle has warnings or errors detected.");

						sendError(check.getResult());
					}
				} else {
					LOGGER.error("Checker init error");
				}
			}

		} else {
			LOGGER.error("received hook without repository");
		}
	}

	/**
	 * @param checkResult Check result
	 */
	private void sendError(final String checkResult) {
		RuntimeException runtimeException = new RuntimeException(checkResult);
		StackTraceElement[] stack = {};
		runtimeException.setStackTrace(stack);
		throw runtimeException;
	}

	/**
	 * Gets files from repository that have changed.
	 * 
	 * @param repository Repository
	 * @param changedFiles changed files
	 */
	private void checkCode(final Repository repository, final Iterable<String> changedFiles) {
		LOGGER.info("check code for changed repository {}", repository.getName());

		RepositoryService repositoryService = null;

		try {
			repositoryService = repositoryServiceFactory.create(repository);

			CatCommandBuilder cat = repositoryService.getCatCommand();

			for (String file : changedFiles) {
				checkCode(cat, file);
			}
			// running the thread of checkstyle.
			check.setBaseDir(System.getProperty("java.io.tmpdir"));
			check.run();

		} finally {
			try {
				Closeables.close(repositoryService, true);
			} catch (IOException ex) {
				LOGGER.warn("could not close repository service {}", ex);
			}
		}

	}

	/**
	 * Checks code from the pushed file.
	 * 
	 * @param cat CatCommandBuilder
	 * @param file File-Path
	 */
	private void checkCode(final CatCommandBuilder cat, final String file) {
		LOGGER.info("check code {}", file);

		try {

			String content = cat.setRevision(newRevision).getContent(file);
			File tempFile = createTempFile(file, content);

			if (tempFile != null) {
				check.addFile(tempFile);
			}
		} catch (Exception ex) {
			LOGGER.warn("could not check code for file ".concat(file), ex);
		}
	}

	/**
	 * Collect modified files.
	 * 
	 * @param repository Repository
	 * @param changesets Collection of change sets
	 * 
	 * @return Changed files
	 */
	private Iterable<String> collectModifiedFiles(final Repository repository, final Collection<Changeset> changesets) {
		LOGGER.info("collect changed files for {}", repository.getName());

		return FluentIterable.from(changesets).transformAndConcat(new Function<Changeset, Iterable<String>>() {

			@Override
			public Iterable<String> apply(final Changeset changeset) {
				newRevision = changeset.getId();
				List<String> changedFiles = new ArrayList<String>();
				Modifications modifications = changeset.getModifications();

				changedFiles.addAll(modifications.getAdded());
				changedFiles.addAll(modifications.getModified());

				changedFiles = (List<String>) removeDuplicates(changedFiles);

				// ignore removed files
				return changedFiles;
			}
		});
	}

	/**
	 * Create a Temporary-file.
	 * 
	 * @param filePath File-Path
	 * @param fileContent Content of file
	 * 
	 * @return Temp-File
	 */
	private File createTempFile(final String filePath, final String fileContent) {
		LOGGER.debug("create temporary file {}", filePath);

		File tempFile = null;
		try {
			String name = new File(filePath).getName();
			String[] str = name.split("\\.");
			if (str.length >= 2) {
				String prefix = str[0];
				String suffix = ".".concat(str[1]);
				// only Java-Files
				if (suffix.equals(".java")) {
					tempFile = new File(System.getProperty("java.io.tmpdir") + prefix + suffix);
					tempFile.deleteOnExit();
					FileWriter fw = new FileWriter(tempFile);
					fw.write(fileContent);
					fw.close();
				}
			}

		} catch (IOException ex) {
			LOGGER.error("could not create a temp file{}", ex);
		}
		return tempFile;
	}

	/**
	 * Remove duplicates.
	 * 
	 * @param changedFiles Files
	 * @return Files without duplicate
	 */
	private Iterable<String> removeDuplicates(final Iterable<String> changedFiles) {

		List<String> files = new LinkedList<String>();
		for (String file : changedFiles) {
			if (!files.contains(file)) {
				files.add(file);
			}
		}
		return files;
	}
}
