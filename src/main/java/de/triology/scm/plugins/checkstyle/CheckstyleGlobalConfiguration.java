/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.checkstyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is an Object that holds all configuration properties and values.
 * 
 * @author Ahmed Saad, Triology GmbH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "checkstyle-configuration")
public class CheckstyleGlobalConfiguration extends CheckstyleConfiguration {

	/** the LOGGER for CheckstyleConfiguration. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckstyleGlobalConfiguration.class);

	/** a path of configuration file for testing Checkstyle. */
	@XmlElement(name = "configPathTest")
	private String configPathTest = "";

	/** a path of source to check with Checkstyle. */
	@XmlElement(name = "srcPath")
	private String srcPath = "";

	/** Field description. */
	@XmlElement(name = "disable-repository-configuration")
	private boolean disableRepositoryConfiguration = false;

	/**
	 * @return a path of Checkstyle config-file for testing.
	 */
	public final String getConfigPathTest() {
		return configPathTest;
	}

	/**
	 * @return a path of source to check with Checkstyle.
	 */
	public final String getSrcPath() {
		return srcPath;
	}

	/**
	 * @return is the repository configuration disabled?
	 */
	public final boolean isDisableRepositoryConfiguration() {
		return disableRepositoryConfiguration;
	}

}
