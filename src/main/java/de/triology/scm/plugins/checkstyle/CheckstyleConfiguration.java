/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.checkstyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.Validateable;
import sonia.scm.repository.Repository;
import sonia.scm.util.Util;

/**
 * This is an Object that holds all configuration properties and values.
 * 
 * @author Ahmed Saad, Triology GmbH
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckstyleConfiguration implements Validateable {

	/** Property name: Path of config-file of Checkstyle. */
	public static final String PROPERTY_CONFIG_PATH = "checkstyle.configPath";

	/** Property name: push reaction of Checkstyle. */
	public static final String PROPERTY_PUSH_REACTION = "checkstyle.pushReaction";

	/** the LOGGER for CheckstyleConfiguration. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckstyleConfiguration.class);

	// ~--- fields ---------------------------------------------------------------

	/** a path of configuration file of Checkstyle. */
	@XmlElement(name = "configPath")
	private String configPath = "";

	/** what should trigger the check-in. */
	@XmlElement(name = "pushReaction")
	private String pushReaction = CheckInReaction.NO_CHECK.name();

	// ~--- constructors ---------------------------------------------------------

	/**
	 * Default constructor. Should only be used by jaxb.
	 */
	public CheckstyleConfiguration() {
	}

	/**
	 * Constructs and initialize the configuration object.
	 * 
	 * @param repository The current repository to get properties from.
	 */
	public CheckstyleConfiguration(final Repository repository) {
		LOGGER.trace("Construct configuration object from repository " + repository.getName());
		configPath = repository.getProperty(PROPERTY_CONFIG_PATH);
		pushReaction = repository.getProperty(PROPERTY_PUSH_REACTION);
	}

	// ~--- get methods ----------------------------------------------------------
	/**
	 * @return a path of Checkstyle config-file .
	 */
	public final String getConfigPath() {
		return configPath;
	}

	/**
	 * @return reaction of checkstyle on check-in.
	 */
	public final String getPushReaction() {
		return pushReaction;
	}

	@Override
	public final boolean isValid() {
		return Util.isNotEmpty(configPath) && Util.isNotEmpty(pushReaction);
	}
}
