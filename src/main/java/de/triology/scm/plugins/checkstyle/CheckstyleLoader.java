/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.checkstyle;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puppycrawl.tools.checkstyle.Checker;
import com.puppycrawl.tools.checkstyle.ConfigurationLoader;
import com.puppycrawl.tools.checkstyle.api.AuditEvent;
import com.puppycrawl.tools.checkstyle.api.AuditListener;
import com.puppycrawl.tools.checkstyle.api.CheckstyleException;
import com.puppycrawl.tools.checkstyle.api.Configuration;
import com.puppycrawl.tools.checkstyle.api.SeverityLevel;

/**
 * Class for run Checkstyle.
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
public class CheckstyleLoader extends Thread implements AuditListener {

	/** the logger for CheckstyleLoader. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckstyleLoader.class);

	/** The check is done. */
	private static final String CHECK_DONE = "Check done.";

	/** The check is started. */
	private static final String STARTED = "Checkstyle started...";

	/** The check is started. */
	private static final String DIV_LINE = "---------------------------------------------";

	/** No errors on check. */
	public static final int NO_ERRORS = 0;
	/** Warnings. */
	public static final int WARNINGS = 1;
	/** Errors. */
	public static final int ERRORS = 2;

	/** End state of Check. */
	private int state = NO_ERRORS;

	/** cushion for avoiding StringBuffer.expandCapacity. */
	private static final int BUFFER_CUSHION = 12;

	/** Checker. */
	private Checker checker;

	/** List of files. */
	private final List<File> files;

	/** List of file names. */
	private String fileNames = "";

	/** Checkstyle result. */
	private String result = "";

	/**
	 * Constructor.
	 * 
	 * @param confPath configuration file
	 * @param srcPath path of source directory
	 */
	public CheckstyleLoader(final String confPath, final String srcPath) {
		setName("Checkstyle");
		files = getJavaFiles(srcPath);
		initChecker(confPath);
	}

	/**
	 * Constructor.
	 * 
	 * @param confPath configuration file
	 * @param pFiles Files
	 */
	public CheckstyleLoader(final String confPath, final List<File> pFiles) {
		setName("Checkstyle");
		files = pFiles;
		initChecker(confPath);
	}

	/**
	 * @param confFile configuration file
	 */
	private void initChecker(final String confFile) {
		final Configuration config;
		try {
			config = ConfigurationLoader.loadConfiguration(confFile, null);
			checker = createChecker(config, this);

		} catch (CheckstyleException cse) {
			LOGGER.error("Error loading configuration file: {}", cse.getMessage());
		}
	}

	/**
	 * Create a new checker with new configuration and AuditListner.
	 * 
	 * @param aConfig Configuration
	 * @param aNosy AuditListner
	 * @return Checker
	 */
	private static Checker createChecker(final Configuration aConfig, final AuditListener aNosy) {
		Checker c = null;

		try {
			c = new Checker();

			final ClassLoader moduleClassLoader = Checker.class.getClassLoader();
			c.setModuleClassLoader(moduleClassLoader);
			c.configure(aConfig);
			c.addListener(aNosy);
		} catch (final Exception e) {
			LOGGER.error("Unable to create Checker: {}", e.getMessage());
		}
		return c;
	}

	/** {@inheritDoc} */
	@Override
	public final void run() {
		checker.process(files);
	}

	/**
	 * Add files to check.
	 * 
	 * @param file File to add
	 */
	public final void addFile(final File file) {
		files.add(file);
	}

	// implement the AuditListener Interface
	/** {@inheritDoc} */
	@Override
	public final void addError(final AuditEvent aEvt) {
		final SeverityLevel severityLevel = aEvt.getSeverityLevel();
		if (!SeverityLevel.IGNORE.equals(severityLevel)) {

			final String fileName = aEvt.getFileName();
			final String message = aEvt.getMessage();

			// avoid StringBuffer.expandCapacity
			final int bufLen = fileName.length() + message.length() + BUFFER_CUSHION;
			final StringBuffer sb = new StringBuffer(bufLen);

			sb.append(fileName);
			sb.append(" :").append(aEvt.getLine());
			if (aEvt.getColumn() > 0) {
				sb.append(':').append(aEvt.getColumn());
			}
			if (SeverityLevel.WARNING.equals(severityLevel)) {
				sb.append(": warning");
			}
			sb.append(": ").append(message);
			addToResult(sb.toString());
			setEndState(ERRORS);
		}
	}

	/** {@inheritDoc} */
	@Override
	public final void addException(final AuditEvent evt, final Throwable t) {
		synchronized (result) {
			addToResult("Error auditing " + evt.getFileName());
			addToResult(t.getMessage());
		}
	}

	/** {@inheritDoc} */
	@Override
	public final void auditStarted(final AuditEvent evt) {
		addToResult("");
		addToResult(DIV_LINE);
		addToResult(STARTED);
		addToResult("");
		setEndState(NO_ERRORS);
	}

	/** {@inheritDoc} */
	@Override
	public final void auditFinished(final AuditEvent evt) {
		addToResult("");
		addToResult(CHECK_DONE);
		addToResult(DIV_LINE);
	}

	/** {@inheritDoc} */
	@Override
	public final void fileStarted(final AuditEvent evt) {
	}

	/** {@inheritDoc} */
	@Override
	public final void fileFinished(final AuditEvent evt) {
	}

	/**
	 * @return the checker
	 */
	public final Checker getChecker() {
		return checker;
	}

	/**
	 * @param pChecker the checker to set.
	 */
	public final void setChecker(final Checker pChecker) {
		checker = pChecker;
	}

	/**
	 * @return the result
	 */
	public final String getResult() {
		return result;
	}

	/**
	 * @param dir the base directory to strip off in filenames
	 */
	public final void setBaseDir(final String dir) {
		checker.setBasedir(dir);
	}

	/**
	 * @return the state
	 */
	public final int getEndState() {
		return state;
	}

	/**
	 * @param pState the state to set
	 */
	public final void setEndState(final int pState) {
		this.state = pState;
	}

	/**
	 * @param line The line to add
	 */
	private void addToResult(final String line) {
		result += line + "\n";
	}

	/**
	 * @return the fileNames
	 */
	public final String getFileNames() {
		return fileNames;
	}

	/**
	 * @param pFileNames the fileNames to set
	 */
	public final void setFileNames(final String pFileNames) {
		fileNames = pFileNames;
	}

	/**
	 * Get from source path an list of Java files.
	 * 
	 * @param srcPath path of source directory
	 * @return List of java files
	 */
	private List<File> getJavaFiles(final String srcPath) {
		List<File> fileList = new LinkedList<File>();
		for (File file : getFiles(srcPath)) {
			if (file.isFile() && file.getName().endsWith(".java")) {
				fileList.add(file);
				fileNames += file.getName() + "\n";
			}
		}
		return fileList;
	}

	/**
	 * Gets file or if directory all containing files from the path.
	 * 
	 * @param srcPath Path of a file or directory
	 * @return List of files
	 */
	private File[] getFiles(final String srcPath) {
		File src = new File(srcPath);
		File[] srcfiles = new File[1];
		if (src.isDirectory()) {
			srcfiles = src.listFiles();
		} else {
			srcfiles[0] = new File(srcPath);
		}
		return srcfiles;
	}

}
