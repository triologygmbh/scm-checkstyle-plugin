/**
 * Copyright (c) 2013, Triology GmbH All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution. 3. Neither the
 * name of SCM-Manager; nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

Ext.ns('Triology.checkstyle');

Triology.checkstyle.I18n = {

    titleText : 'Checkstyle Configuration',
    formTitleText : 'Checkstyle',

    repositoryConfigurationText : 'Do not allow repository configuration',
    repositoryConfigurationHelpText : 'Do not allow repository owners to configure checkstyle instances. \n\
	    You have to restart your SCM-Manager after changing this value.',

    pushReactionText : 'Reaction on Push',
    pushReactionHelpText : 'Select how to react to a push.',

    configPathText : 'Config-File',
    configPathHelpText : 'Path of checkstyle config-file.',

    configPathTestText : 'Config-File for testing',
    configPathTestHelpText : 'Path of checkstyle config-file for testing Checkstyle.',

    srcPathText : 'Path of files to test',
    srcPathHelpText : 'Path of source-code file or directory to check his Java-files.',

    btnTestcheck : 'Checkstyle Test',
    btnTestcheckHelpText : 'Checks all .java files in the giving directory and show a result if exists.\n\
		Note: You have to save changes bevore check your files',

    comboNoCheck : 'Do not check',
    comboCheck : 'Check and warn',
    comboCheckAndBlock : 'Check and block',

    resultWindowTitle : 'Checkstyle Result',
    resultWindowMsg : 'Output:',

    loadingText : 'Load ...',
    submitText : 'Submit ...',
    checkText : 'Check ...',

    // errors
    errorBoxTitle : 'Error',
    errorOnSubmitText : 'Error during config submit.',
    errorValidSubmitText : 'Make sure that your entry is valid.',
    errorOnLoadText : 'Error during config load.',
    errorOnTestText : 'Login failed.',
};

// German translation
switch (i18n.country) {
case 'de':
    Triology.checkstyle.I18n = {

	titleText : 'Checkstyle Einstellungen',
	formTitleText : 'Checkstyle',

	repositoryConfigurationText : 'Repository-Einstellungen verbieten',
	repositoryConfigurationHelpText : 'Verhindert, dass Repository-Besitzer eigene Checkstyle-Einstellungen vornehmen können. \n\
		Der SCM-Manager muss nach einer Änderung neu gestartet werden.',

	configPathText : 'Config-Datei',
	configPathHelpText : 'Pfad für die Config-Datei eingeben',

	pushReactionText : 'Verhalten beim pushen',
	pushReactionHelpText : 'Bestimmt das Verhalten von Checkstyle bei einem Push.',

	configPathTestText : 'Config-Datei zum Testen',
	configPathTestHelpText : 'Pfad für die Config-Datei eingeben, womit die Checkstyle-Einstellung getestet werden soll.',

	srcPathText : 'Ordner-Pfad der Dateien',
	srcPathHelpText : 'Pfad der Java-Datei oder des Ordners eingeben, der die Java-Dateien zum Testen enthält',

	btnTestcheck : 'Checkstyle Test',
	btnTestcheckHelpText : 'Checkstyle durchsucht .java Dateien im angegebenen Ordner und gibt das Resultat des Checks aus, falls vorhanden. \n\
		                Notiz: Speichern Sie die Änderungen vor dem Check.',

	comboNoCheck : 'Nicht prüfen',
	comboCheck : 'Prüfen und warnen',
	comboCheckAndBlock : 'Prüfen und anhalten',

	resultWindowTitle : 'Checkstyle-Ergebnis',
	resultWindowMsg : 'Ausgabe:',

	loadingText : 'Laden ...',
	submitText : 'Senden ...',
	checkText : 'Prüfen ...',

	// errors
	errorBoxTitle : 'Fehler',
	errorOnSubmitText : 'Ein Fehler ist bei der Übermittlung der Daten aufgetreten.',
	errorValidSubmitText : 'Bitte Eingaben auf Korrektheit überprüfen.',
	errorOnLoadText : 'Fehler beim Laden der Konfigurations-Werte.',
	errorOnTestText : 'Anmeldeversuch fehlgeschlagen!',
    };
    break;

case 'fr':
    Triology.checkstyle.I18n = {

	titleText : 'Paramètre de Checkstyle',
	formTitleText : 'Checkstyle',

	repositoryConfigurationText : 'Empêche les paramètres du référentiel',
	repositoryConfigurationHelpText : 'Empêche le propriétaire du référentiel de configurer ses propres paramètres.\n\
											Le SCM-Manager doit être redémarré après se changement.',

	configPathText : 'Fichier de configuration',
	configPathHelpText : 'Entrez le chemin du fichier de configuration de Checkstyle.',

	pushReactionText : 'Comportement lors de l\'ajout',
	pushReactionHelpText : 'Déterminée le comportement de Checkstyle lors de l\'ajout.',

	configPathTestText : 'Fichier de configuration pour le test',
	configPathTestHelpText : 'Entrez le chemin du fichier de configuration pour tester Checkstyle.',

	srcPathText : 'Fichiers a tester',
	srcPathHelpText : 'Entrez le chemin du fichier ou dossier à parcourir pour tester ses fichier Java.',

	btnTestcheck : 'Tester Checkstyle',
	btnTestcheckHelpText : 'Checkstyle vérifie les fichiers .java dans le dossier spécifié et renvoie le résultat de la vérification, en cas échéant. \n\
			Remarque: Enregistrez les modifications avant le test.',

	comboNoCheck : 'Ne pas vérifier',
	comboCheck : 'Vérifier et avertir',
	comboCheckAndBlock : 'Vérifier et arrêter',

	resultWindowTitle : 'Résultat de Checkstyle',
	resultWindowMsg : 'Sortie:',

	loadingText : 'chargement ...',
	submitText : 'envoi ...',
	checkText : 'Vérifier ...',

	// errors
	errorBoxTitle : 'Erreur',
	errorOnSubmitText : 'Une erreur s\'est produite lors de la transmission de données.',
	errorValidSubmitText : 'Vérifiez l\'exactitude de vos données.',
	errorOnLoadText : 'Erreur lors chargement des Paramètre.',
	errorOnTestText : 'Connexion a échoué!'
    };
    break;
}