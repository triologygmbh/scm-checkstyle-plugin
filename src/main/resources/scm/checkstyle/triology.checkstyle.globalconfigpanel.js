/**
 * Copyright (c) 2013, Triology GmbH All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution. 3. Neither the
 * name of SCM-Manager; nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

Ext.ns("Triology.checkstyle");
Triology.checkstyle.GlobalConfigPanel = Ext.extend(Sonia.config.ConfigForm, {

    initComponent : function() {
	var config = {
	    title : Triology.checkstyle.I18n.titleText,
	    items : [{
				id : 'disable-repository-configuration',
				name : 'disable-repository-configuration',
				xtype : 'checkbox',
				inputValue : 'true',
				fieldLabel : Triology.checkstyle.I18n.repositoryConfigurationText,
				helpText : Triology.checkstyle.I18n.repositoryConfigurationHelpText,
				listeners : {
					check : {
					scope : this,
					fn : this.dis_enableConfig
					}
			    }
		    }, {
				id : 'configPath',
				name : 'configPath',
				xtype : 'textfield',
				allowBlank : false,
				fieldLabel : Triology.checkstyle.I18n.configPathText,
				helpText : Triology.checkstyle.I18n.configPathHelpText
		    }, {
				id : 'pushReaction',
				name : 'pushReaction',
				xtype : 'combo',
				property : 'checkstyle.pushReaction',
				fieldLabel : Triology.checkstyle.I18n.pushReactionText,
				helpText : Triology.checkstyle.I18n.pushReactionHelpText,
				valueField : 'value',
				displayField : 'displayText',
				typeAhead : false,
				editable : false,
				triggerAction : 'all',
				mode : 'local',
				store : new Ext.data.SimpleStore({
				    fields : [ 'value', 'displayText' ],
				    data : [ [ 'NO_CHECK', Triology.checkstyle.I18n.comboNoCheck ],
					    [ 'CHECK_AND_WARN', Triology.checkstyle.I18n.comboCheck ],
					    [ 'CHECK_AND_BLOCK', Triology.checkstyle.I18n.comboCheckAndBlock ] ]
			})
		    }, {
				xtype : 'spacer',
				height : 20
		    }, {
				xtype : 'label',
				text : Triology.checkstyle.I18n.btnTestcheck + ':',
				style : 'font-weight:bold;',
		    }, {
				xtype : 'spacer',
				height : 10
		    }, {
				id : 'configPathTest',
				name : 'configPathTest',
				xtype : 'textfield',
				fieldLabel : Triology.checkstyle.I18n.configPathTestText,
				helpText : Triology.checkstyle.I18n.configPathTestHelpText
		    }, {
				id : 'srcPath',
				name : 'srcPath',
				xtype : 'textfield',
				fieldLabel : Triology.checkstyle.I18n.srcPathText,
				helpText : Triology.checkstyle.I18n.srcPathHelpText
		    }, {
				xtype : 'button',
				text : Triology.checkstyle.I18n.btnTestcheck,
				helpText : Triology.checkstyle.I18n.btnTestcheckHelpText,
				fieldLabel : ' ',
				scope : this,
				handler : function() {
				    this.runCheckstyle();
				}
		    } ]

	};

	Ext.apply(this, Ext.apply(this.initialConfig, config));
	Triology.checkstyle.GlobalConfigPanel.superclass.initComponent.apply(this, arguments);
    },

    runCheckstyle : function() {
	this.el.mask(Triology.checkstyle.I18n.checkText);
	Ext.Ajax.request({
	    url : restUrl + 'plugins/checkstyle/test',
	    method : 'GET',
	    scope : this,
	    disableCaching : true,
	    success : function(response) {
		this.el.unmask();
		var trace = response.responseText;
		new Triology.checkstyle.MultilineWindow({
		    title : Triology.checkstyle.I18n.resultWindowTitle,
		    message : Triology.checkstyle.I18n.resultWindowMsg,
		    stacktrace : trace
		}).show();
	    },
	    failure : function() {
		this.el.unmask();
		Ext.MessageBox.show({
		    title : Triology.checkstyle.I18n.errorBoxTitle,
		    msg : Triology.checkstyle.I18n.errorOnLoadText,
		    buttons : Ext.MessageBox.OK,
		    icon : Ext.MessageBox.ERROR
		});
	    }
	});
    },

    onSubmit : function(values) {
	var form = this.getForm();
	if (form.isValid()) {
	    this.el.mask(Triology.checkstyle.I18n.submitText);
	    var pushReaction = Ext.getCmp('pushReaction').getValue();
	    values['pushReaction'] = pushReaction;
	    Ext.Ajax.request({
		url : restUrl + 'plugins/checkstyle/global-config.json',
		method : 'POST',
		jsonData : values,
		scope : this,
		disableCaching : true,
		success : function(response) {
		    this.el.unmask();
		},
		failure : function() {
		    this.el.unmask();
		    Ext.MessageBox.show({
			title : Triology.checkstyle.I18n.errorBoxTitle,
			msg : Triology.checkstyle.I18n.errorOnSubmitText,
			buttons : Ext.MessageBox.OK,
			icon : Ext.MessageBox.ERROR
		    });
		}
	    });
	} else {
	    // TODO: mask the save button.
	    Ext.MessageBox.show({
		title : Triology.checkstyle.I18n.errorBoxTitle,
		msg : Triology.checkstyle.I18n.errorValidSubmitText,
		buttons : Ext.MessageBox.OK,
		icon : Ext.MessageBox.ERROR
	    });
	}
    },

    onLoad : function(el) {
	var tid = setTimeout(function() {
	    el.mask(Triology.checkstyle.I18n.loadingText);
	}, 100);
	Ext.Ajax.request({
	    url : restUrl + 'plugins/checkstyle/global-config.json',
	    method : 'GET',
	    scope : this,
	    disableCaching : true,
	    success : function(response) {
		var obj = Ext.decode(response.responseText);
		this.load(obj);
		clearTimeout(tid);
		el.unmask();
	    },
	    failure : function() {
		clearTimeout(tid);
		el.unmask();
		Ext.MessageBox.show({
		    title : Triology.checkstyle.I18n.errorBoxTitle,
		    msg : Triology.checkstyle.I18n.errorOnLoadText,
		    buttons : Ext.MessageBox.OK,
		    icon : Ext.MessageBox.ERROR
		});
	    }
	});
	this.dis_enableConfig(Ext.getCmp('disable-repository-configuration'));
    },
    
    dis_enableConfig : function(checkbox){
	    var cmpConfigPath=Ext.getCmp('configPath');
		var cmpPushReaction=Ext.getCmp('pushReaction');
		if (!checkbox.getValue()) {
	    	cmpConfigPath.disable();
	    	cmpPushReaction.disable();
		} else {
		    cmpConfigPath.enable();
		    cmpPushReaction.enable();
		}
	}
});

Ext.reg("checkstyleGlobalConfigPanel", Triology.checkstyle.GlobalConfigPanel);

// register global config panel
registerGeneralConfigPanel({
    id : 'checkstyleGlobalConfigPanel',
    xtype : 'checkstyleGlobalConfigPanel'
});
