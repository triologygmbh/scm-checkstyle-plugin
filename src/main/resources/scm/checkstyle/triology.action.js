/**
 * Copyright (c) 2013, Triology GmbH All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer. 2. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution. 3. Neither the name of SCM-Manager;
 * nor the names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

Ext.ns('Triology.checkstyle');
Triology.checkstyle.MultilineWindow = Ext.extend(Ext.Window, {

	title : null,
	message : null,
	stacktrace : null,
	icon : Ext.MessageBox.INFO,

	// labels
	okText : 'Ok',
	exceptionText : 'Result',

	initComponent : function() {
		var config = {
			title : this.title,
			width : 640,
			height : 400,
			closable : true,
			resizable : true,
			plain : true,
			border : false,
			modal : true,
			bodyCssClass : 'x-window-dlg',
			items : [ {
				id : 'stacktraceArea',
				xtype : 'textarea',
				editable : false,
				fieldLabel : this.exceptionText,
				value : this.stacktrace,
				width : '98%',
				height : 335,
				hidden : false
			} ],
			buttons : [ {
				text : this.okText,
				scope : this,
				handler : this.close
			} ]
		};

		Ext.apply(this, Ext.apply(this.initialConfig, config));
		Triology.checkstyle.MultilineWindow.superclass.initComponent.apply(this, arguments);
	}
});
