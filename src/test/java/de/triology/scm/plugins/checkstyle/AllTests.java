package de.triology.scm.plugins.checkstyle;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Runs all the defined test cases.
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({ CheckLoaderTest.class })
public class AllTests {

}
