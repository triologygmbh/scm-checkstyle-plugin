package de.triology.scm.plugins.checkstyle;

/**
 * Class to run Checkstyle
 */

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

/**
 * @author asaad
 */
public class CheckLoaderTest {

	/**
	 * 
	 */
	@Test
	public final void startCheck() {
		List<File> files = new LinkedList<File>();
		File[] dir = new File("src/main/java/de/triology/scm/plugins/checkstyle").listFiles();
		for (File file : dir) {
			files.add(file);
		}
		CheckstyleLoader check = new CheckstyleLoader("sun_checks.xml", files);
		check.start();

		org.junit.Assert.assertNotNull("output not available", check.getResult());
	}
}
